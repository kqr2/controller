######################################
# Define build type variables
######################################
DEBUG = 1	# 1: debug build, 0: release build
OPT = -Og	# optimization level (0g, 00, 01, 02, 03, 0s).

######################################
# sources
######################################
CPP_SOURCES =  \
$(APP_SOURCES) \
$(CFG_SOURCES) \
$(DRIVER_SOURCES) \
$(LIB_SOURCES) \

C_SOURCES =  \
$(APP_C_SOURCES) \
$(MCU_C_SOURCES) \

ASM_SOURCES =  \
$(MCU_ASSEMBLY) \

#######################################
# Compiler Tools
#######################################
COMPILER_FAMILY := arm-none-eabi-

# The gcc compiler bin path can be either defined in make command via GCC_PATH variable (> make GCC_PATH=xxx)
# either it can be added to the PATH environment variable.
ifdef GCC_PATH
	PREFIX = $(GCC_PATH)/$(COMPILER_FAMILY)
else
	PREFIX = $(COMPILER_FAMILY)
endif

CPP = $(PREFIX)g++
CC = $(PREFIX)gcc
AS = $(PREFIX)gcc -x assembler-with-cpp
CP = $(PREFIX)objcopy
SZ = $(PREFIX)size
HEX = $(CP) -O ihex
BIN = $(CP) -O binary -S
 
#######################################
# MCU specific flags
#######################################
CPU = -mcpu=cortex-m4
FPU = -mfpu=fpv4-sp-d16
FLOAT-ABI = -mfloat-abi=hard

# Build the MCU flag variable
MCU = $(CPU) -mthumb $(FPU) $(FLOAT-ABI)

#######################################
# Symbol Definitions
#######################################
COMMON_DEFS =  \
-DUSE_HAL_DRIVER \
-DSTM32L475xx
-D__FPU_PRESENT=1U

CPP_DEFS =  \
$(COMMON_DEFS)

C_DEFS =  \
$(COMMON_DEFS)

AS_DEFS = 

#######################################
# Include Paths
#######################################
CPP_INCLUDES =  \
$(APP_INCLUDES) \
$(CFG_INCLUDES) \
$(DRIVER_INCLUDES) \
$(HAL_INCLUDES) \
$(LIB_INCLUDES) \
$(MCU_INCLUDES) \

C_INCLUDES =  \
-Ifirmware/stm32/Core/Inc \
-Ifirmware/stm32/Drivers/STM32L4xx_HAL_Driver/Inc \
-Ifirmware/stm32/Drivers/STM32L4xx_HAL_Driver/Inc/Legacy \
-Ifirmware/stm32/Drivers/CMSIS/Device/ST/STM32L4xx/Include \
-Ifirmware/stm32/Drivers/CMSIS/Include \
-Ifirmware/stm32/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 \
-Ifirmware/stm32/Middlewares/Third_Party/FreeRTOS/Source/include \
-Ifirmware/stm32/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F

AS_INCLUDES = 

#######################################
# Compiler Flag Definitions
#######################################
WFLAGS := -Wall #-Wextra
COMMON_FLAGS := -fdata-sections -ffunction-sections
DEBUG_FLAGS := -g3 -gdwarf-2

CPPFLAGS += $(MCU) $(CPP_DEFS) $(CPP_INCLUDES) $(OPT) $(WFLAGS) $(COMMON_FLAGS)
CPPFLAGS += -std=c++17 -fno-rtti -fno-exceptions -lstdc++ $(DEBUG_FLAGS) 

CFLAGS += $(MCU) $(C_DEFS) $(C_INCLUDES) $(OPT) $(WFLAGS) $(COMMON_FLAGS) $(DEBUG_FLAGS) 
ASFLAGS = $(MCU) $(AS_DEFS) $(AS_INCLUDES) $(OPT) $(WFLAGS) $(COMMON_FLAGS)

# Generate dependency information
CFLAGS += -MMD -MP -MF"$(@:%.o=%.d)"
CPPFLAGS += -MMD -MP -MF"$(@:%.o=%.d)"

#######################################
# Linker Setup and Flags
#######################################
LDSCRIPT = $(LINKER_DIR)/STM32L475VGTx_FLASH.ld
LIBDIR = \

LDLIBS = -lc -lm -lnosys  
# Note: we could conditionally add the float library in based on a USE_FLOAT flag
LDLIBS += -u _printf_float

LDFLAGS = $(MCU) -specs=nano.specs -specs=nosys.specs -T$(LDSCRIPT) $(LIBDIR) $(LDLIBS) -Wl,-Map=$(EXE_DIR)/$(TARGET).map,--cref -Wl,--gc-sections

#######################################
# Targets
#######################################
all: $(EXE_DIR)/$(TARGET).elf $(EXE_DIR)/$(TARGET).hex $(EXE_DIR)/$(TARGET).bin

$(shell mkdir -p $(BUILD_DIR))
$(shell mkdir -p $(EXE_DIR))

# list of C objects
OBJECTS = $(addprefix $(BUILD_DIR)/,$(notdir $(C_SOURCES:.c=.o)))
vpath %.c $(sort $(dir $(C_SOURCES)))

# list of C++ objects
OBJECTS += $(addprefix $(BUILD_DIR)/,$(notdir $(CPP_SOURCES:.cpp=.o)))
vpath %.cpp $(sort $(dir $(CPP_SOURCES)))

# list of ASM program objects
OBJECTS += $(addprefix $(BUILD_DIR)/,$(notdir $(ASM_SOURCES:.s=.o)))
vpath %.s $(sort $(dir $(ASM_SOURCES)))

$(BUILD_DIR)/%.o: %.cpp Makefile | $(BUILD_DIR)
	$(CPP) -c $(CPPFLAGS) -Wa,-a,-ad,-alms=$(BUILD_DIR)/$(notdir $(<:.cpp=.lst)) $< -o $@

$(BUILD_DIR)/%.o: %.c Makefile | $(BUILD_DIR)
	$(CC) -c $(CFLAGS) -Wa,-a,-ad,-alms=$(BUILD_DIR)/$(notdir $(<:.c=.lst)) $< -o $@

$(BUILD_DIR)/%.o: %.s Makefile | $(BUILD_DIR)
	$(AS) -c $(CFLAGS) $< -o $@

$(EXE_DIR)/$(TARGET).elf: $(OBJECTS) Makefile | $(BUILD_DIR)
	$(CPP) $(OBJECTS) $(LDFLAGS) -o $@

$(EXE_DIR)/%.hex: $(EXE_DIR)/%.elf | $(BUILD_DIR)
	$(HEX) $< $@

$(EXE_DIR)/%.bin: $(EXE_DIR)/%.elf | $(BUILD_DIR)
	$(BIN) $< $@
	$(SZ) $(EXE_DIR)/$(TARGET).elf

#######################################
# dependencies
#######################################
-include $(wildcard $(BUILD_DIR)/*.d)

# *** EOF ***