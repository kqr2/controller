#include "CppUTest/TestHarness.h"

extern "C"
{
	/*
	 * Add your c-only include files here
	 */
    #include "adder.h"
}

TEST_GROUP(adder)
{
    void setup()
    {

    }

    void teardown()
    {
    }
};

TEST(adder, smallResult)
{
    CHECK_EQUAL(2, add(1, 1));
}

