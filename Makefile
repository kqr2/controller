export

# Define what we are building
TARGET = controller

# Build input directories 
FIRMWARE_DIR := firmware
APP_DIR := $(FIRMWARE_DIR)/app
CFG_DIR := $(FIRMWARE_DIR)/cfg
DRIVER_DIR := $(FIRMWARE_DIR)/drv
LINKER_DIR := $(FIRMWARE_DIR)/ld
LIB_DIR := $(FIRMWARE_DIR)/lib
MCU_DIR := $(FIRMWARE_DIR)/stm32
TEST_DIR := tests
COVERAGE_DIR := $(TEST_DIR)/coverage


# Build output directores
BUILD_DIR := $(FIRMWARE_DIR)/build
EXE_DIR := bin
ANALYSIS_DIR := analytics

# List of the application headers and sources. The APP code should be 
# hardware independent so that it can be used on-target, in simulation,
# and in unit tests. C sources are included to allow for the use of
# vendor C code. 
APP_HEADERS := $(wildcard $(APP_DIR)/*.h*)
APP_SOURCES := $(wildcard $(APP_DIR)/*.cpp)
APP_C_SOURCES := $(wildcard $(APP_DIR)/*.c)
APP_ASSEMBLY := $(wildcard $(APP_DIR)/*.[sS])
APP_INCLUDES := $(patsubst %,-I%,$(sort $(dir $(APP_HEADERS))))

# List of the configuration headers and sources. The CFG code should be
# hardware dependent and should be able to be used on-target.
CFG_HEADERS := $(wildcard $(CFG_DIR)/*.h)
CFG_HEADERS += $(wildcard $(CFG_DIR)/*.hpp)
CFG_SOURCES := $(wildcard $(CFG_DIR)/*.c)
CFG_SOURCES += $(wildcard $(CFG_DIR)/*.cpp)
CFG_INCLUDES := $(patsubst %,-I%,$(sort $(dir $(CFG_HEADERS))))

# List of the driver headers and sources. The DRIVER code should be
# hardware dependent and should be able to be used on-target.
DRIVER_HEADERS := $(wildcard $(DRIVER_DIR)/*.h)
DRIVER_HEADERS += $(wildcard $(DRIVER_DIR)/*.hpp)
DRIVER_SOURCES := $(wildcard $(DRIVER_DIR)/*.c)
DRIVER_SOURCES += $(wildcard $(DRIVER_DIR)/*.cpp)
DRIVER_ASSEMBLY := $(wildcard $(DRIVER_DIR)/*.[sS])
DRIVER_INCLUDES := $(patsubst %,-I%,$(sort $(dir $(DRIVER_HEADERS))))

# List of the library headers and sources. The LIB code should be
# hardware independent and should be able to be used on-target.
LIB_HEADERS := $(wildcard $(LIB_DIR)/*.h*)
LIB_SOURCES := $(wildcard $(LIB_DIR)/*.c)
LIB_SOURCES += $(wildcard $(LIB_DIR)/*.cpp)
LIB_INCLUDES := $(patsubst %,-I%,$(sort $(dir $(LIB_HEADERS))))

# List of the microcontroller headers and sources. The MCU code should be
# hardware dependent and should be able to be used on-target.
# NOTE: The following looks messy because of how STM32CubeMx generates code
MCU_HEADERS   := $(shell find $(MCU_DIR) -type f -name '*.h*')#$(wildcard $(MCU_DIR)/*.h*)
MCU_SOURCES   := $(wildcard $(MCU_DIR)/*.cpp)
MCU_C_SOURCES := $(wildcard $(MCU_DIR)/Core/Src/*.c)
MCU_C_SOURCES += $(filter-out %template.c, $(wildcard $(MCU_DIR)/Drivers/STM32L4xx_HAL_Driver/Src/*.c))
MCU_C_SOURCES += $(wildcard $(MCU_DIR)/Middlewares/Third_Party/FreeRTOS/Source/*.c)
MCU_C_SOURCES += $(wildcard $(MCU_DIR)/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2/*.c)
MCU_C_SOURCES += $(wildcard $(MCU_DIR)/Middlewares/Third_Party/FreeRTOS/Source/portable/MemMang/heap_4.c)
MCU_C_SOURCES += $(wildcard $(MCU_DIR)/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F/*.c)
MCU_ASSEMBLY  := $(wildcard $(MCU_DIR)/*.[sS])
MCU_INCLUDES := $(patsubst %,-I%,$(sort $(dir $(MCU_HEADERS))))

# List of the test headers and sources. The TEST code should be
# hardware independent and should be able to be used in the test harness
TEST_HEADERS := $(wildcard $(TEST_DIR)/*.h*)
TEST_SOURCES := $(wildcard $(TEST_DIR)/*.c)
TEST_SOURCES += $(wildcard $(TEST_DIR)/*.cpp)
TEST_INCLUDES := $(patsubst %,-I%,$(sort $(dir $(TEST_HEADERS))))

# Gcov and Gcovr flags
GCOVR_EXCLUDE_DIR = '($(TEST_DIR)|$(LIB_DIR))'
GCOVR_EXCLUDE_FLAG := --exclude $(GCOVR_EXCLUDE_DIR)
GCOVR_FLAGS := $(GCOVR_EXCLUDE_FLAG) --txt --html-details --html=$(COVERAGE_DIR)/coverage.html


#######################################
# build targets
#######################################

.PHONY: all clean

all: release debug

clean:
	-rm -fR $(BUILD_DIR)
	-rm -fR $(EXE_DIR)

release debug:
	$(MAKE) -j -f $(FIRMWARE_DIR)/$@.mk

.PHONY: unit_tests
unit_tests: 
	$(MAKE) -j CC=gcc -f $(TEST_DIR)/cpputest.mk
	mkdir -p $(COVERAGE_DIR)
	@gcovr $(GCOVR_FLAGS)

.PHONY: mccabe_analysis
mccabe_analysis:
	@pmccabe -vt $(APP_SOURCES) $(APP_C_SOURCES)

.PHONY: docker_image
docker_image:
	docker build -t kqr2/embedded-dev .

.PHONY: docker_run
docker_run:
	docker run --rm -it --privileged \
	-v "$$(pwd):/home/app" \
	-v "/dev/bus/usb:/dev/bus/usb" \
	kqr2/embedded-dev:latest bash

#######################################
# dependencies
#######################################
-include $(wildcard $(BUILD_DIR)/*.d)

# *** EOF ***