# By setting CMAKE_SYSTEM_NAME to "Generic," you indicate to CMake that it should avoid platform-specific
# configurations and try to generate a more generic build system that can be used across different
# environments. This is often done in cross-compilation scenarios or when building code that is intended to
# be platform-independent.
set(CMAKE_SYSTEM_NAME Generic)

# Set the toolchain base path
set(TOOLCHAIN_BASE_PATH "/home/dev/gcc-arm-none-eabi-10-2020-q4-major/bin/")

# Specify the cross compiler and associated tools
set(CMAKE_C_COMPILER "${TOOLCHAIN_BASE_PATH}arm-none-eabi-gcc")
set(CMAKE_CXX_COMPILER "${TOOLCHAIN_BASE_PATH}arm-none-eabi-g++")
set(CMAKE_ASM_COMPILER "${TOOLCHAIN_BASE_PATH}arm-none-eabi-gcc")
set(OBJCOPY_PATH "${TOOLCHAIN_BASE_PATH}arm-none-eabi-objcopy")
set(SIZE_TOOL "${TOOLCHAIN_BASE_PATH}arm-none-eabi-size")

# MCU specific option flags
# We use set to create a list of flags that we want to pass to the compiler. It is a list of strings. 
# This is a convenient and configurable method. The flags we are passing to the compiler include:
#
# -mcpu=cortex-m4:    Specifies the target CPU architecture as Cortex-M4.
# -mthumb:            Indicates that the code should be compiled for the Thumb instruction set, which is
#                     commonly used in ARM-based microcontrollers for code size optimization.
# -mfpu=fpv4-sp-d16:  Specifies the FPU (Floating-Point Unit) type for Cortex-M4, in this case, "fpv4-sp-d16"
#                     stands for a single-precision FPU with 16 double-precision registers.
# -mfloat-abi=hard:   Specifies that the code should use the "hard" floating-point ABI (Application Binary Interface),
#                     which means that floating-point calculations should be performed using hardware instructions
#                     (as opposed to software emulation).
# --specs=nano.specs: This flag is specific to some ARM toolchains (like the GCC ARM toolchain) and is used to
#                     specify linker options for using the Nano Standard C Library, which is a minimalistic
#                     version of the C library optimized for embedded systems with limited resources.
set(MCU_FLAGS
    -mcpu=cortex-m4
    -mthumb
    -mfpu=fpv4-sp-d16
    -mfloat-abi=hard
    -specs=nano.specs
)

# In our configuration with multiple toolchains and languages, CMake struggles a bit processing the
# MCU_FLAGS list. To work around this, we convert the list to a single string with a space delimiter.
string(JOIN " " MCU_FLAGS_STRING "${MCU_FLAGS}")
string(REPLACE ";" " " MCU_FLAGS_STRING "${MCU_FLAGS_STRING}")

# Define additional compiler symbols (-D)
#
# USE_HAL_DRIVER:     Symbol used to tell STM32 library we are using the HAL drivers
# STM32L475xx:        Symbol specifying the MCU target family 
# __FPU_PRESENT       Tell the ST libraries that the FPU is present
add_compile_definitions(
    USE_HAL_DRIVER
    STM32L475xx
    __FPU_PRESENT=1U
)

# Common flags for all compilers
#
# g:                  Generate debug information
# O2:                 Optimize code for speed
# Wall:               Enables a compiler warning messages that catch common programming errors
# Wextra:             Enables some extra warning flags that are not enabled by -Wall
# Werror:             Make all warnings into errors
# pedantic:           Issue all the warnings demanded by strict ISO C and ISO C++
# Wunused-variable:   Warn whenever a local variable or non-constant static variable is unused aside from its
# Wuninitialized:     Warn if an automatic variable is used without first being initialized
# Wshadow:            Warn whenever a local variable or type declaration shadows another variable
# fstack-protector:   Enable stack protection checks
# Wconversion:        Warn for implicit conversions that may alter a value
# Wunused-function:   Warn whenever a static function is declared but not defined or a non-inline static function
# funsigned-char:     Treat character data type as unsigned instead of signed
# fdata-sections:     Instructs the compiler to place each global or static variable in a separate data section
# ffunction-sections: Instructs the compiler to place each function in a separate code section
set(COMMON_FLAGS
    -g
    -O2
    -Wall
#    -Wextra
#    -Werror
#    -pedantic
    -Wunused-variable
    -Wuninitialized
#    -Wshadow
    -fstack-protector
#    -Wconversion
    -Wunused-function
    -funsigned-char
    -fdata-sections
    -ffunction-sections
)

string(REPLACE ";" " " COMMON_FLAGS "${COMMON_FLAGS}")

# C++ flags
#
# std:                Let's use C++17
# fno-rtti:           Disable Run-Time Type Information. Decreases binary size and improves performance
# fno-exceptions:     For microcontrollers, we disable exceptions due to run-time overhead and dynamic allocation
# lstdc++:            C++ should link our application
set(CPP_ONLY_FLAGS
    -std=c++17
    -fno-rtti
    -fno-exceptions
    -lstdc++
)

string(REPLACE ";" " " CPP_ONLY_FLAGS "${CPP_ONLY_FLAGS}")

# C flags
#
# std:                Let's use C11
set(C_ONLY_FLAGS
    -std=c11
)

string(REPLACE ";" " " C_ONLY_FLAGS "${C_ONLY_FLAGS}")

# Set compiler-specific flags
set(CMAKE_CXX_FLAGS "${MCU_FLAGS_STRING} ${COMMON_FLAGS} ${CPP_ONLY_FLAGS}")
set(CMAKE_C_FLAGS "${MCU_FLAGS_STRING} ${COMMON_FLAGS} ${C_ONLY_FLAGS}")
set(CMAKE_ASM_FLAGS "${MCU_FLAGS_STRING} ${COMMON_FLAGS}")

# Link script
set(LDSCRIPT ${CMAKE_CURRENT_SOURCE_DIR}/firmware/ld/STM32L475VGTx_FLASH.ld)


# Linker options
#
# -T:                 Linker script
# -specs=nosys.specs: Linker options for using the Nano Standard C Library
# -Wl,-Map:           Generate a map file
# -Wl,--cref:         Add cross reference to the map file
# -Wl,--gc-sections:  Remove unused sections from the final binary
set(TOOLCHAIN_LINKER_OPTIONS
    -T${LDSCRIPT}
    -specs=nosys.specs
    -Wl,-Map=${EXE_DIR}/${TARGET}.map,--cref
    -Wl,--gc-sections
)

# Libraries to link
#
# c:                  The C library
# m:                  The math library
# nosys:              The Nano Standard C Library
set(TOOLCHAIN_LIBRARIES
    c
    m
    nosys
)

set(CMAKE_TRY_COMPILE_TARGET_TYPE "STATIC_LIBRARY")