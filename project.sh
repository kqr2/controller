#!/bin/bash

BUILD_ROOT_DIR="build"
TOOLCHAIN_FILE="toolchain-arm.cmake" # Default toolchain

if [ "$1" = "erase-all" ]; then
    rm -rf $BUILD_ROOT_DIR
    exit 0
fi

case "$1" in
    release)
        BUILD_SUB_DIR="release"
        BUILD_TYPE="Release"
        ;;
    simulation)
        BUILD_SUB_DIR="simulation"
        BUILD_TYPE="Simulation"
        TOOLCHAIN_FILE="toolchain-host.cmake" # Use the host toolchain for simulation
        ;;
    debug|*)
        BUILD_SUB_DIR="debug"
        BUILD_TYPE="Debug"
        ;;
esac

BUILD_DIR="$BUILD_ROOT_DIR/$BUILD_SUB_DIR"

case "$2" in
    clean)
        ninja -C $BUILD_DIR clean
        ;;
    erase)
        rm -rf $BUILD_DIR
        ;;
    build|*)
        if [ ! -d "$BUILD_DIR" ]; then
            cmake -G Ninja -B $BUILD_DIR -S . -DCMAKE_BUILD_TYPE=$BUILD_TYPE -DCMAKE_TOOLCHAIN_FILE=$TOOLCHAIN_FILE
        fi
        ninja -C $BUILD_DIR
        ;;
    verbose)
        ninja -C $BUILD_DIR -v
        ;;
esac