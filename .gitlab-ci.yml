# This file is a template, and might need editing before it works on your project.
# To contribute improvements to CI/CD templates, please follow the Development guide at:
# https://docs.gitlab.com/ee/development/cicd/templates.html
# This specific template is located at:
# https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Getting-Started.gitlab-ci.yml

# This is a sample GitLab CI/CD configuration file that should run without any modifications.
# It demonstrates a basic 3 stage CI/CD pipeline. Instead of real tests or scripts,
# it uses echo commands to simulate the pipeline execution.
#
# A pipeline is composed of independent jobs that run scripts, grouped into stages.
# Stages run in sequential order, but jobs within stages run in parallel.
#
# For more information, see: https://docs.gitlab.com/ee/ci/yaml/index.html#stages

default:
  image: kqr2/embedded-dev:latest 

stages:          # List of stages for jobs, and their order of execution
  - build
  - analysis
  - test
  - deploy
  - release

.standard-rules:       # Make a hidden job to hold the common rules
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - if: $CI_COMMIT_TAG

build-job:       # This job runs in the build stage, which runs first.
  stage: build
  script:
    - echo "Compiling the code..."
    # - make release
    - sh project.sh release
    - echo "Compile complete."
  artifacts:
    paths:
      - build/release/bin/controller.bin
      - build/release/bin/controller.elf
      - build/release/bin/controller.hex
    expire_in: 1 week    

analysis-job:   # This job runs in the analysis stage, which runs second.
  stage: analysis
  script:
    - echo "Analyzing software..."   

mccabe-job:
  stage: analysis
  script:
    - echo "Calculating McCabe complexity . . ."
    - mkdir -p build/analysis/
    - make mccabe_analysis > build/analysis/mccabe.txt
  artifacts:
    paths:
      - build/analysis/mccabe.txt
    expire_in: 1 week     

unit-test-job:   # This job runs in the test stage.
  stage: test    # It only starts when the job in the build stage completes successfully.
  script:
    - make unit_tests
    - tar -czf test_coverage.tar.gz tests/coverage
    - echo "TEST_JOB_ID=$CI_JOB_ID" >> test.env
  artifacts:
    reports: #Save job id for later use, do not export
      dotenv: test.env
    paths:
    - tests/coverage/
    - test_coverage.tar.gz 

lint-test-job:   # This job also runs in the test stage.
  stage: test    # It can run at the same time as unit-test-job (in parallel).
  script:
    - echo "Linting code... This will take about 10 seconds."
    - sleep 10
    - echo "No lint issues found."

deploy-job:      # This job runs in the deploy stage.
  stage: deploy  # It only runs when *both* jobs in the test stage complete successfully.
  script:
    - echo "Deploying application..."
    - find . -name "controller.hex"
    -  st-flash --reset --format ihex write ./build/release/bin/controller.hex
    - echo "Application successfully deployed."
  tags:
    - deploy
  dependencies:
    - build-job      

release_job:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest # Docker image with cli for making releases : https://docs.gitlab.com/ee/user/project/releases/release_cli.html
  rules:
    - if: $CI_COMMIT_TAG  # Run this job when a tag is created
  script:
    - echo "running release_job"
  release:  # See https://docs.gitlab.com/ee/ci/yaml/#release for available properties
    tag_name: '$CI_COMMIT_TAG'
    description: '$CI_COMMIT_TAG'
    assets:
      links:
      - name: 'code-complexity report'
        url: '${CI_PROJECT_URL}/-/jobs/artifacts/$CI_COMMIT_REF_NAME/raw/build/analysis/mccabe.txt?job=mccabe-job'
      - name: 'test-coverage-report'
        url: '${CI_PROJECT_URL}/-/jobs/${TEST_JOB_ID}/artifacts/file/test_coverage.tar.gz'
      - name: 'binaries'
        url: '${CI_PROJECT_URL}/-/jobs/artifacts/$CI_COMMIT_REF_NAME/raw/build/release/bin/controller.hex?job=build-job'        

